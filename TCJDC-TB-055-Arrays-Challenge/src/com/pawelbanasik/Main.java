package com.pawelbanasik;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		int[] myArray = getIntegers(5);
		printArray(myArray);
		System.out.println();
		printArray(sortArray(myArray));
	}

	public static int[] getIntegers(int number) {
		System.out.println("Enter " + number + " integer values:");

		int[] array = new int[number];
		for (int i = 0; i < array.length; i++) {
			array[i] = sc.nextInt();

		}
		return array;

	}

	public static void printArray(int[] array) {

		for (int i = 0; i < array.length; i++) {
			System.out.println("Element with reference " + i + " is " + array[i]);

		}
	}

	public static int[] sortArray(int[] array) {
		// int max = Integer.MAX_VALUE;
		Arrays.sort(array);

		int[] sortedFromMax = new int[array.length];
		for (int i = 0; i < array.length; i++) {

			sortedFromMax[i] = array[array.length - 1 - i];
		}

		return sortedFromMax;
	}
}
